use core::cmp::Reverse;
use std::ffi::OsString;
use std::io::{self, Write};
use std::os::unix::ffi::OsStrExt;
use std::path::{Path, PathBuf};
use std::time::SystemTime;
use std::{env, fs};

fn mtime<P: AsRef<Path>>(entry: P) -> SystemTime {
    entry.as_ref().metadata().unwrap().modified().unwrap()
}

fn path_to_lower(path: &Path) -> OsString {
    path.as_os_str().to_ascii_lowercase()
}

fn sort_by_mtime_or_abc(entries: &mut [PathBuf], sorting: &Sorting) {
    match sorting {
        Sorting::AbcFrw => entries.sort_unstable_by(|a, b| {
            alphanumeric_sort::compare_os_str(&path_to_lower(a), &path_to_lower(b))
        }),
        Sorting::AbcBkwr => entries.sort_unstable_by(|a, b| {
            alphanumeric_sort::compare_os_str(&path_to_lower(b), &path_to_lower(a))
        }),
        Sorting::MtimeBkwr => {
            entries.sort_unstable_by(|a, b| (mtime(b), Reverse(b)).cmp(&(mtime(a), Reverse(a))))
        }
    }
}

fn search_inside(entry_point: &Path, can_be_target: bool, sorting: &Sorting) -> Option<PathBuf> {
    if let Ok(dir_reader) = fs::read_dir(&entry_point) {
        let mut subdirs: Vec<PathBuf> = Vec::new();

        for entry in dir_reader.flatten() {
            let path = entry.path();
            if can_be_target && path.is_file() {
                return Some(entry_point.to_path_buf());
            } else if path.is_dir() {
                subdirs.push(entry.path());
            }
        }
        sort_by_mtime_or_abc(&mut subdirs, sorting);
        for subdir in subdirs {
            if let Some(matched) = search_inside(&subdir, true, sorting) {
                return Some(matched);
            }
        }
    }
    None
}

fn search_beside(mut entry_point: &Path, sorting: &Sorting) -> Option<PathBuf> {
    loop {
        if let Some(mut siblings) = get_siblings(entry_point) {
            siblings.retain(|s| match sorting {
                Sorting::AbcFrw => s.as_path() >= entry_point,
                Sorting::AbcBkwr => s.as_path() <= entry_point,
                _ => (mtime(s), Reverse(s.as_path())) <= (mtime(entry_point), Reverse(entry_point)),
            });
            sort_by_mtime_or_abc(&mut siblings, sorting);
            for sibling in siblings {
                if let Some(matched) = search_inside(&sibling, true, sorting) {
                    return Some(matched);
                }
            }
        };

        match entry_point.parent() {
            Some(parent) => entry_point = parent,
            None => break None,
        };
    }
}

fn get_siblings(entry_point: &Path) -> Option<Vec<PathBuf>> {
    if let Some(parent) = entry_point.parent() {
        if let Ok(dir_reader) = fs::read_dir(&parent) {
            let siblings: Vec<PathBuf> = dir_reader
                .flatten()
                .map(|e| e.path())
                .filter(|p| p.is_dir() && p != entry_point)
                .collect();

            if !siblings.is_empty() {
                return Some(siblings);
            }
        }
    }
    None
}

fn write_path(path: &Path, writer: &mut impl Write) -> io::Result<()> {
    writer.write_all(path.as_os_str().as_bytes())?;
    Ok(())
}

enum Sorting {
    AbcFrw,
    AbcBkwr,
    MtimeBkwr,
}

fn main() -> io::Result<()> {
    let args: Vec<_> = env::args_os().skip(1).collect();
    if args.len() != 2 {
        eprintln!("Expected two arguments: <sorting-mode> <directory>");
        std::process::exit(1);
    };

    let sorting = match args[0].as_bytes() {
        b"abc-frw" => Sorting::AbcFrw,
        b"abc-bkwr" => Sorting::AbcBkwr,
        _ => Sorting::MtimeBkwr,
    };

    let mut writer = io::BufWriter::with_capacity(8192, io::stdout());

    let mut start = PathBuf::from(&args[1]);
    loop {
        if start.is_dir() {
            match search_inside(&start, false, &sorting) {
                Some(path) => write_path(&path, &mut writer)?,
                None => {
                    if let Some(path) = search_beside(&start, &sorting) {
                        write_path(&path, &mut writer)?;
                    }
                }
            }
            break;
        } else {
            start = start.parent().unwrap().to_path_buf();
        }
    }
    writer.flush().unwrap();
    Ok(())
}
